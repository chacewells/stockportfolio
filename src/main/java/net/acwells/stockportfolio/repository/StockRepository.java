package net.acwells.stockportfolio.repository;

import net.acwells.stockportfolio.model.Stock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface StockRepository extends CrudRepository<Stock, String> {
  @Query("SELECT s.symbol from Stock s")
  Collection<String> findAllSymbols();
}
