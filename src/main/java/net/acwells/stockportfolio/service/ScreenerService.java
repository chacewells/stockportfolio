package net.acwells.stockportfolio.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.acwells.stockportfolio.client.FinancialModelingPrepClient;
import net.acwells.stockportfolio.dto.StockAnalysisDTO;
import net.acwells.stockportfolio.exchange.IncomeStatement;
import net.acwells.stockportfolio.exchange.StockAnalysisUpdate;
import net.acwells.stockportfolio.model.FundamentalData;
import net.acwells.stockportfolio.model.Stock;
import net.acwells.stockportfolio.repository.StockRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScreenerService {
  private final FinancialModelingPrepClient financialModelingPrepClient;
  private final StockRepository stockRepository;
  private final ObjectMapper objectMapper;

  @Transactional
  public void updateFundamentalDataBySymbol(String symbol) {
    StockAnalysisUpdate stockAnalysisUpdate = financialModelingPrepClient.retrieveStockAnalysisUpdate(symbol);
    Stock stock = stockRepository.findById(symbol).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    FundamentalData fundamentalData = stock.getFundamentalData();
    if (fundamentalData == null) {
      fundamentalData = FundamentalData.builder().symbol(symbol).stock(stock).build();
      stock.setFundamentalData(fundamentalData);
    }

    fundamentalData.setDebtEquityRatio(stockAnalysisUpdate.getDebtToEquity());
    fundamentalData.setTangibleBookValuePerShare(stockAnalysisUpdate.getTangibleBookValuePerShare());
    fundamentalData.setPrice(stockAnalysisUpdate.getStockPrice());
    fundamentalData.setMarketCap(stockAnalysisUpdate.getMarketCapitalization());
    fundamentalData.setCurrentDividend(stockAnalysisUpdate.getCurrentDividend());

    SortedMap<Integer, BigDecimal> netIncomeHistory = fundamentalData.getNetIncomeHistory();
    for (IncomeStatement incomeStatement : stockAnalysisUpdate.getIncomeStatements()) {
      int year = incomeStatement.getDate().getYear();
      netIncomeHistory.put(year, incomeStatement.getNetIncome());
    }
    stockRepository.save(stock);
  }

  public List<StockAnalysisDTO> findAll() {
    return mapToStockAnalysisList(stockRepository.findAll());
  }

  public void updateAllFundamentals() {
    stockRepository.findAllSymbols().forEach(this::updateFundamentalDataBySymbol);
  }

  private List<StockAnalysisDTO> mapToStockAnalysisList(Iterable<Stock> stocks) {
    ArrayList<StockAnalysisDTO> stockAnalysisList = new ArrayList<>();
    for (Stock stock : stocks) {
      stockAnalysisList.add(mapToStockAnalysis(stock));
    }

    return stockAnalysisList;
  }

  private StockAnalysisDTO mapToStockAnalysis(Stock stock) {
    FundamentalData fundamentalData = stock.getFundamentalData();
    if (fundamentalData == null) {
      return StockAnalysisDTO.builder()
          .symbol(stock.getSymbol())
          .build();
    }

    StockAnalysisDTO stockAnalysisDTO = objectMapper.convertValue(fundamentalData, StockAnalysisDTO.class);
    SortedMap<Integer, BigDecimal> netIncomeHistory = fundamentalData.getNetIncomeHistory();
    if (!netIncomeHistory.isEmpty()) {
      BigDecimal earliestNetIncome = netIncomeHistory.get(netIncomeHistory.firstKey());
      BigDecimal currentNetIncome = netIncomeHistory.get(netIncomeHistory.lastKey());

      stockAnalysisDTO.setCurrentNetIncome(currentNetIncome);
      stockAnalysisDTO.setFiveYearEarningsGrowth(currentNetIncome.subtract(earliestNetIncome));
      stockAnalysisDTO.setPriceToTangibleBookValueRatio(fundamentalData.getPrice().divide(fundamentalData.getTangibleBookValuePerShare(), 2, RoundingMode.HALF_EVEN));
    }

    return stockAnalysisDTO;
  }
}
