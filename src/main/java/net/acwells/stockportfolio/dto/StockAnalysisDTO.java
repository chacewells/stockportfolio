package net.acwells.stockportfolio.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StockAnalysisDTO {
  private String symbol;
  private String marketCap;
  private BigDecimal price;
  private BigDecimal debtEquityRatio;
  private BigDecimal currentNetIncome;
  private BigDecimal currentDividend;
  private BigDecimal fiveYearEarningsGrowth;
  private BigDecimal priceToTangibleBookValueRatio;
}
