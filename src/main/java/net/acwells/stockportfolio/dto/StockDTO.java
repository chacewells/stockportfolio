package net.acwells.stockportfolio.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockDTO {
  private String symbol;
  private String companyName;
  private String sector;
  private String industry;
  private String url;
  private String description;
}
