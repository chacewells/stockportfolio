package net.acwells.stockportfolio.exchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StockAnalysisUpdate {
    private BigDecimal stockPrice;
    private BigDecimal tangibleBookValuePerShare;
    private BigDecimal debtToEquity;
    private IncomeStatement[] incomeStatements;
    private BigDecimal marketCapitalization;
    private BigDecimal currentDividend;
}
