package net.acwells.stockportfolio.exchange;

import lombok.Data;

@Data
public class StockDataRoot {
    private StockData data;
    private String message;
    private int status;
}
