package net.acwells.stockportfolio.exchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class EnterpriseValue {
    private BigDecimal stockPrice;
    private BigDecimal marketCapitalization;
}
