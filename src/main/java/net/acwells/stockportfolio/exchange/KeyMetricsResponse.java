package net.acwells.stockportfolio.exchange;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class KeyMetricsResponse {
    private BigDecimal debtToEquity;
    private BigDecimal tangibleBookValuePerShare;
}
