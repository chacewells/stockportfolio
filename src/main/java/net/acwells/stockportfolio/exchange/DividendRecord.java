package net.acwells.stockportfolio.exchange;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class DividendRecord {
    private LocalDate date;
    private BigDecimal dividend;
}
