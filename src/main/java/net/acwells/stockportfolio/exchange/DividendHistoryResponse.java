package net.acwells.stockportfolio.exchange;

import lombok.Data;

import java.util.List;

@Data
public class DividendHistoryResponse {
    private String symbol;
    private List<DividendRecord> historical;
}
