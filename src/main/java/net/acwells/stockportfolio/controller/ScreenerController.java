package net.acwells.stockportfolio.controller;

import lombok.RequiredArgsConstructor;
import net.acwells.stockportfolio.service.ScreenerService;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/screener")
@RequiredArgsConstructor
public class ScreenerController {
  private final ScreenerService screenerService;

  @GetMapping
  public String getScreenerList(Model model) {
    model.addAttribute("analysisList", screenerService.findAll());
    return "screener/index";
  }

  @GetMapping("/refresh/{symbol}")
  public String refreshScreenerBySymbol(@PathVariable String symbol, @RequestHeader(HttpHeaders.REFERER) String referer) {
    screenerService.updateFundamentalDataBySymbol(symbol);
    return "redirect:" + referer;
  }

  @GetMapping("/refresh")
  public String refreshAll(@RequestHeader(HttpHeaders.REFERER) String referer) {
    screenerService.updateAllFundamentals();
    return "redirect:" + referer;
  }

}
