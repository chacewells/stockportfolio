package net.acwells.stockportfolio.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.acwells.stockportfolio.client.YahooFinanceApiClient;
import net.acwells.stockportfolio.dto.StockAnalysisDTO;
import net.acwells.stockportfolio.dto.StockDTO;
import net.acwells.stockportfolio.exchange.StockDataRoot;
import net.acwells.stockportfolio.model.FundamentalData;
import net.acwells.stockportfolio.model.Stock;
import net.acwells.stockportfolio.repository.StockRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Map.Entry;
import java.util.SortedMap;

import static org.springframework.util.StringUtils.hasText;

@RequiredArgsConstructor
@Controller
@RequestMapping("/stocks")
@Slf4j
public class StockController {
  private final StockRepository stockRepository;
  private final YahooFinanceApiClient yahooFinanceApiClient;
  private final ObjectMapper objectMapper;

  @GetMapping
  public String listStocks(Model model) {
    Iterable<Stock> allStocks = stockRepository.findAll();
    model.addAttribute("title", "My Portfolio");
    model.addAttribute("stocks", mapToDtoList(allStocks));
    return "stocks/index";
  }

  @GetMapping("/import")
  public String importStockForm(Model model) {
    StockDTO stock = new StockDTO();
    model.addAttribute("title", "Import Stock");
    model.addAttribute("stock", stock);
    return "stocks/import";
  }

  @PostMapping
  @Transactional
  public String importStock(StockDTO stockDTO) {
    if (stockRepository.existsById(stockDTO.getSymbol())) {
      return "redirect:/stocks/" + stockDTO.getSymbol();
    }

    StockDataRoot stockInfo = yahooFinanceApiClient.getStockInfo(stockDTO.getSymbol());
    Stock importedStock = Stock.builder()
            .symbol(stockDTO.getSymbol())
            .companyName(stockInfo.getData().getLongName())
            .url(stockInfo.getData().getWebsite())
            .sector(stockInfo.getData().getSector())
            .industry(stockInfo.getData().getIndustry())
            .description(stockInfo.getData().getLongBusinessSummary())
            .build();
    stockRepository.save(importedStock);
    return "redirect:/stocks/" + importedStock.getSymbol();
  }

  @GetMapping("/{symbol}/edit")
  public String editStock(Model model, @PathVariable String symbol) {
    Stock stock = stockRepository.findById(symbol).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    model.addAttribute("title", "Edit Stock: " + symbol);
    model.addAttribute("stock", stock);
    model.addAttribute("editing", true);
    return "stocks/edit";
  }

  @GetMapping("/{symbol}")
  public String viewStock(Model model, @PathVariable String symbol) {
    Stock stock = stockRepository.findById(symbol).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    StockDTO stockDTO = mapToDto(stock);

    model.addAttribute("title", "Stock: " + symbol);
    model.addAttribute("stock", stockDTO);
    model.addAttribute("editing", false);
    return "stocks/edit";
  }

  @PostMapping("/{symbol}")
  @Transactional
  public String updateStock(StockDTO stockDTO, @PathVariable String symbol) {
    log.info("Stock requested for update: {}", stockDTO);
    performUpdate(symbol, stockDTO);
    return "redirect:/stocks";
  }

  private void performUpdate(String symbol, StockDTO stockDTO) {
    Stock stockToUpdate = stockRepository.findById(symbol).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

    if (hasText(stockDTO.getCompanyName())) {
      stockToUpdate.setCompanyName(stockDTO.getCompanyName());
    }
    if (hasText(stockDTO.getSector())) {
      stockToUpdate.setSector(stockDTO.getSector());
    }
    if (hasText(stockDTO.getIndustry())) {
      stockToUpdate.setIndustry(stockDTO.getIndustry());
    }
    if (hasText(stockDTO.getUrl())) {
      stockToUpdate.setUrl(stockDTO.getUrl());
    }
    if (hasText(stockDTO.getDescription())) {
      stockToUpdate.setDescription(stockDTO.getDescription());
    }
  }

  private Iterable<StockDTO> mapToDtoList(Iterable<Stock> stock) {
    return objectMapper.convertValue(stock, new TypeReference<>() {
    });
  }

  private StockDTO mapToDto(Stock stock) {
    return objectMapper.convertValue(stock, StockDTO.class);
  }

}
