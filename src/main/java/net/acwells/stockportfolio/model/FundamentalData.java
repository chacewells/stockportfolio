package net.acwells.stockportfolio.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import java.math.BigDecimal;
import java.util.SortedMap;
import java.util.TreeMap;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class FundamentalData {
  @Id
  @Column(name = "symbol")
  private String symbol;

  private BigDecimal marketCap;

  private BigDecimal price;

  private BigDecimal debtEquityRatio;

  @Builder.Default
  @ElementCollection(fetch = FetchType.EAGER)
  @MapKeyColumn(name = "statement_year")
  @Column(name = "net_income")
  @CollectionTable(name = "net_income_history", joinColumns = @JoinColumn(name = "symbol", referencedColumnName = "symbol"))
  @OrderBy("statement_year ASC")
  private SortedMap<Integer, BigDecimal> netIncomeHistory = new TreeMap<>();

  private BigDecimal currentDividend;

  private BigDecimal tangibleBookValuePerShare;

  @OneToOne
  @MapsId
  @JoinColumn(name = "symbol")
  @JsonIgnore
  private Stock stock;
}
