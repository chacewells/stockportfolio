package net.acwells.stockportfolio.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class Stock {
    @Id
    private String symbol;

    private String companyName;

    private String sector;

    private String industry;

    private String url;

    @Lob
    private String description;

    @OneToOne(mappedBy = "stock", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private FundamentalData fundamentalData;
}
