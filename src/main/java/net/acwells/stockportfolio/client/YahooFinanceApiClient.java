package net.acwells.stockportfolio.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.acwells.stockportfolio.exchange.StockDataRoot;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class YahooFinanceApiClient {
  public static final String ROOT_URI = "https://yahoo-finance97.p.rapidapi.com";
  private final RestTemplate restTemplate;
  private final ObjectMapper objectMapper;

  public YahooFinanceApiClient(RestTemplateBuilder restTemplateBuilder, @Value("${rapid-api.api-key}") String rapidApiKey, ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
    this.restTemplate = restTemplateBuilder
        .rootUri(ROOT_URI)
        .defaultHeader("content-type", "application/x-www-form-urlencoded")
        .defaultHeader("X-RapidAPI-Host", "yahoo-finance97.p.rapidapi.com")
        .defaultHeader("X-RapidAPI-Key", rapidApiKey)
        .build();
  }

  @SneakyThrows
  public StockDataRoot getStockInfo(String symbol) {
    StockDataRoot stockDataRoot = restTemplate.postForObject("/stock-info", "symbol=" + symbol, StockDataRoot.class);
    log.info("stockDataRoot = {}", objectMapper.writeValueAsString(stockDataRoot));
    return stockDataRoot;
  }
}
