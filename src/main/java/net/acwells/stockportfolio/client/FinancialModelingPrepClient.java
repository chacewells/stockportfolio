package net.acwells.stockportfolio.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.acwells.stockportfolio.exchange.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class FinancialModelingPrepClient {
    private static final String FMP_API_ROOT = "https://financialmodelingprep.com";

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final String apiKey;

    public FinancialModelingPrepClient(RestTemplateBuilder restTemplateBuilder, ObjectMapper objectMapper, @Value("${fmp.api-key}") String apiKey) {
        this.restTemplate = restTemplateBuilder.rootUri(FMP_API_ROOT).build();
        this.objectMapper = objectMapper;
        this.apiKey = apiKey;
    }

    public StockAnalysisUpdate retrieveStockAnalysisUpdate(String symbol) {
        KeyMetricsResponse keyMetrics = getKeyMetrics(symbol);
        IncomeStatement[] incomeStatementHistory = getIncomeStatementHistory(symbol);
        EnterpriseValue latestEnterpriseValue = getLatestEnterpriseValue(symbol);
        Optional<DividendRecord> latestDividendRecord = getLatestDividendRecord(symbol);

        StockAnalysisUpdate stockAnalysisUpdate = new StockAnalysisUpdate();
        stockAnalysisUpdate.setDebtToEquity(keyMetrics.getDebtToEquity());
        stockAnalysisUpdate.setTangibleBookValuePerShare(keyMetrics.getTangibleBookValuePerShare());
        stockAnalysisUpdate.setIncomeStatements(incomeStatementHistory);
        stockAnalysisUpdate.setStockPrice(latestEnterpriseValue.getStockPrice());
        stockAnalysisUpdate.setMarketCapitalization(latestEnterpriseValue.getMarketCapitalization());
        stockAnalysisUpdate.setCurrentDividend(latestDividendRecord.map(DividendRecord::getDividend).orElse(null));

        return stockAnalysisUpdate;
    }

    private KeyMetricsResponse getKeyMetrics(String symbol) {
        ResponseEntity<KeyMetricsResponse[]> response = restTemplate.getForEntity("/api/v3/key-metrics/{symbol}?limit=1&apikey={apiKey}", KeyMetricsResponse[].class, symbol, apiKey);
        return response.getBody()[0];
    }

    private IncomeStatement[] getIncomeStatementHistory(String symbol) {
        ResponseEntity<IncomeStatement[]> response = restTemplate.getForEntity("/api/v3/income-statement/{symbol}?limit=120&apikey={apiKey}", IncomeStatement[].class, symbol, apiKey);
        return response.getBody();
    }

    private EnterpriseValue getLatestEnterpriseValue(String symbol) {
        ResponseEntity<EnterpriseValue[]> response = restTemplate.getForEntity("/api/v3/enterprise-values/{symbol}?limit=1&apikey={apiKey}", EnterpriseValue[].class, symbol, apiKey);
        return response.getBody()[0];
    }

    private Optional<DividendRecord> getLatestDividendRecord(String symbol) {
        ResponseEntity<DividendHistoryResponse> response = restTemplate.getForEntity("/api/v3/historical-price-full/stock_dividend/{symbol}?apikey={apiKey}", DividendHistoryResponse.class, symbol, apiKey);
        DividendHistoryResponse dividendHistoryResponse = response.getBody();
        return dividendHistoryResponse.getHistorical().stream().findFirst();
    }
}
