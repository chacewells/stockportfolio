package net.acwells.stockportfolio;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.acwells.stockportfolio.model.Stock;
import net.acwells.stockportfolio.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.Arrays;


@Slf4j
@SpringBootApplication
public class StockPortfolioApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockPortfolioApplication.class, args);
    }

    @Value("classpath:stock-samples.json")
    Resource stockSamplesFile;

    @Autowired
    StockRepository stockRepository;

    @Autowired
    ObjectMapper objectMapper;

    @SneakyThrows
    @Bean
    ApplicationRunner loadDb() {
        return args -> {
            Stock[] stocks;
            try (InputStream inputStream = stockSamplesFile.getInputStream()) {
                stocks = objectMapper.readValue(inputStream, Stock[].class);
            }
            for (Stock stock : stocks) {
                if (stock.getFundamentalData() != null) {
                    stock.getFundamentalData().setStock(stock);
                }
            }
            stockRepository.saveAll(Arrays.asList(stocks));
        };
    }

}
